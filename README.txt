# Copyright 2022 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.3.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

**NOTE** This setup is for rootless podman only. *DO NOT* run this as root!
Your user should not have admin privileges.

Read the comments in all the files before you do anything else. They will
tell you what you might need to change. The DEVICE variable in pod.conf is
probably all you need to edit.

./create_server.sh
will pull the latest docker image and make the container.
"-f" will do so even if the container already exists.
"--FORCE" will also *DELETE YOUR DATA VOLUME*. Don't do that unless you
really need to start over completely.

./setup_systemd.sh
will create the necessary systemd files *and start the service*. You can use
"systemd --user start/stop zwave-server" to manually control it.

To start the service on boot, the user needs "linger" enabled.
"sudo loginctl enable-linger <username>"
will do that for you. *Note* that user linger is considered a security risk
by many smart people. Make sure your user does not have admin privileges
(and "sudo" should require a password). Don't run other services as the same
user, unless you really know what you're doing.
