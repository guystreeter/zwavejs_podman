#!/usr/bin/env bash
#
# Copyright 2021-2022 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.3.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

FORCE=${1:-}

set -o nounset
set -o errexit

source ./pod.conf

# "-f" option will create the container evein if it exists
if [ x-fx == x${FORCE}x ]; then
    rm_container ${CONTAINER_NAME}
fi

# "--FORCE" option will create the container
# and **DELETE THE DATA VOLUME!**
if [ x--FORCEx == x${FORCE}x ]; then
    rm_container ${CONTAINER_NAME}
    rm_volume ${DATA_VOL}
fi

# This seems to be the default, but we'll set it just in case
CONTAINER_VOL=/usr/src/app/store
ZWAVEJS_EXTERNAL_CONFIG=${CONTAINER_VOL}/.config-db

# You can pass in SESSION_SECRET also, or you can take care of that
# during startup

podman container exists ${CONTAINER_NAME} && echo "${CONTAINER_NAME} exists!" && exit
podman volume exists ${DATA_VOL} || podman volume create ${DATA_VOL}

# Create the zwave server container
# NOTE: The host user needs to be in the "dialout" group so it can access the
# Zwave USB device. the "--group-add keep-groups" option passes the user's
# additional groups to the container.
podman container create \
    ${SEC_OPT} \
    --group-add keep-groups \
    --pull always \
    --stop-signal=SIGINT \
    ${PORTS} \
    --device ${DEVICE}:rw \
    --volume ${DATA_VOL}:${CONTAINER_VOL} \
    --restart on-failure \
    --name ${CONTAINER_NAME} \
    --env ZWAVEJS_DISABLE_SOFT_RESET=1 \
    --env ZWAVEJS_EXTERNAL_CONFIG=${ZWAVEJS_EXTERNAL_CONFIG} \
    --env TZ=${CONTAINER_TZ} \
    docker.io/zwavejs/zwave-js-ui:latest
