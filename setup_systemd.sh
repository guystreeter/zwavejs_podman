#!/usr/bin/env bash
#
# Copyright 2021-2022 Guy Streeter
#   This copyrighted material is made available to anyone wishing to use,
#  modify, copy, or redistribute it subject to the terms and conditions of
#  the GNU General Public License v.3.
#
#   This application is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
# Authors:
#   Guy Streeter <guy.streeter@gmail.com>
#

# In order for per-user systemd service to start on boot, the user needs
# the "linger" setting enabled.
# "sudo loginctl enable-linger <username>" will do this.

set -o nounset
set -o errexit

source ./pod.conf

# your OS might put these files somewhere else, but probably not
pushd ~/.config/systemd/user/
podman generate systemd -f -n  --pod-prefix '' --container-prefix '' --separator '' ${CONTAINER_NAME}
popd

systemctl --user daemon-reload
systemctl --user enable --now ${CONTAINER_NAME}
